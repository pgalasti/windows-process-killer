﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KillThis.Struture
{
    class RunningProcess
    {
        public RunningProcess(int id, String name)
        {
            this.ID = id;
            this.Name = name;
        }

        public int ID { get; }
        public String Name { get; }
    }
}
