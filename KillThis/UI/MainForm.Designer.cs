﻿namespace KillThis
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.processSearchComboBox = new System.Windows.Forms.ComboBox();
            this.killAllButton = new System.Windows.Forms.Button();
            this.processListBox = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.killButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.stayDeadListBox = new System.Windows.Forms.ListBox();
            this.addToTrackButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Process Name";
            // 
            // processSearchComboBox
            // 
            this.processSearchComboBox.DisplayMember = "Name";
            this.processSearchComboBox.FormattingEnabled = true;
            this.processSearchComboBox.Location = new System.Drawing.Point(12, 25);
            this.processSearchComboBox.Name = "processSearchComboBox";
            this.processSearchComboBox.Size = new System.Drawing.Size(189, 21);
            this.processSearchComboBox.TabIndex = 1;
            this.processSearchComboBox.ValueMember = "Name";
            this.processSearchComboBox.SelectedIndexChanged += new System.EventHandler(this.processSearchComboBox_SelectedIndexChanged);
            this.processSearchComboBox.TextUpdate += new System.EventHandler(this.processSearchComboBox_TextUpdate);
            this.processSearchComboBox.SelectedValueChanged += new System.EventHandler(this.processSearchComboBox_SelectedValueChanged);
            this.processSearchComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.processSearchComboBox_KeyPress);
            // 
            // killAllButton
            // 
            this.killAllButton.BackColor = System.Drawing.Color.Tomato;
            this.killAllButton.Enabled = false;
            this.killAllButton.Location = new System.Drawing.Point(203, 230);
            this.killAllButton.Name = "killAllButton";
            this.killAllButton.Size = new System.Drawing.Size(75, 23);
            this.killAllButton.TabIndex = 2;
            this.killAllButton.Text = "Kill All!";
            this.killAllButton.UseVisualStyleBackColor = false;
            this.killAllButton.Click += new System.EventHandler(this.killAllButton_Click);
            // 
            // processListBox
            // 
            this.processListBox.DisplayMember = "Name";
            this.processListBox.FormattingEnabled = true;
            this.processListBox.Location = new System.Drawing.Point(11, 67);
            this.processListBox.Name = "processListBox";
            this.processListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.processListBox.Size = new System.Drawing.Size(186, 186);
            this.processListBox.TabIndex = 3;
            this.processListBox.ValueMember = "ID";
            this.processListBox.SelectedIndexChanged += new System.EventHandler(this.processListBox_SelectedIndexChanged);
            this.processListBox.SizeChanged += new System.EventHandler(this.processListBox_SizeChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Results";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // killButton
            // 
            this.killButton.BackColor = System.Drawing.Color.Tomato;
            this.killButton.Enabled = false;
            this.killButton.Location = new System.Drawing.Point(203, 201);
            this.killButton.Name = "killButton";
            this.killButton.Size = new System.Drawing.Size(75, 23);
            this.killButton.TabIndex = 6;
            this.killButton.Text = "Kill!";
            this.killButton.UseVisualStyleBackColor = false;
            this.killButton.Click += new System.EventHandler(this.killButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.stayDeadListBox);
            this.groupBox1.Location = new System.Drawing.Point(284, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(124, 241);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Stay Dead List";
            // 
            // stayDeadListBox
            // 
            this.stayDeadListBox.FormattingEnabled = true;
            this.stayDeadListBox.Location = new System.Drawing.Point(7, 26);
            this.stayDeadListBox.Name = "stayDeadListBox";
            this.stayDeadListBox.Size = new System.Drawing.Size(111, 212);
            this.stayDeadListBox.TabIndex = 0;
            // 
            // addToTrackButton
            // 
            this.addToTrackButton.BackColor = System.Drawing.Color.Khaki;
            this.addToTrackButton.Location = new System.Drawing.Point(203, 130);
            this.addToTrackButton.Name = "addToTrackButton";
            this.addToTrackButton.Size = new System.Drawing.Size(75, 23);
            this.addToTrackButton.TabIndex = 8;
            this.addToTrackButton.Text = "Keep Dead";
            this.addToTrackButton.UseVisualStyleBackColor = false;
            this.addToTrackButton.Click += new System.EventHandler(this.addToTrackButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(423, 265);
            this.Controls.Add(this.addToTrackButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.killButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.processListBox);
            this.Controls.Add(this.killAllButton);
            this.Controls.Add(this.processSearchComboBox);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Process Hunter";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox processSearchComboBox;
        private System.Windows.Forms.Button killAllButton;
        private System.Windows.Forms.ListBox processListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button killButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox stayDeadListBox;
        private System.Windows.Forms.Button addToTrackButton;
    }
}

