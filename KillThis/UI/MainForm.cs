﻿using KillThis.Logic;
using KillThis.Struture;
using System;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KillThis
{
    public partial class MainForm : Form
    {
        private ProcessSearch mainProcessSearch = new ProcessSearch();
        private Thread monitorThread;
        public MainForm()
        {
            InitializeComponent();
            monitorThread = new Thread(new ThreadStart(MontiorDeadList));
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            List<RunningProcess> processes = mainProcessSearch.searchProcesses();
            foreach(RunningProcess process in processes)
            {
                processSearchComboBox.Items.Add(process);
            }

            processSearchComboBox.Sorted = true;
            monitorThread.Start();
        }

        private void Search()
        {
            processListBox.Items.Clear();

            String procName = (String)processSearchComboBox.Text;
            ProcessSearch processSearch = new ProcessSearch(procName);

            List<RunningProcess> processes = processSearch.searchProcesses();
            foreach (RunningProcess process in processes)
            {
                processListBox.Items.Add(process);
            }

            processListBox.Sorted = true;
        }

        private void processSearchComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void processSearchComboBox_TextUpdate(object sender, EventArgs e)
        {
            if (processSearchComboBox.Text == String.Empty)
            {
                processListBox.Items.Clear();
                return;
            }
                

            Search();
        }

        private void processListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            killButton.Enabled = false;

            if (processListBox.SelectedItems.Count == 0)
                return;

            killButton.Enabled = true;
        }

        private void processListBox_SizeChanged(object sender, EventArgs e)
        {
            killAllButton.Enabled = false;

            if (processListBox.Items.Count == 0)
                return;

            killAllButton.Enabled = true;
        }

        const int ENTER_KEY = 13;
        private void processSearchComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ENTER_KEY)
                Search();
        }

        private void killButton_Click(object sender, EventArgs e)
        {
            List<RunningProcess> removeThese = new List<RunningProcess>();
            bool failure = false;
            foreach (RunningProcess process in processListBox.SelectedItems)
            {
                try
                {
                    ProcessKiller.KillProcess(process);
                } catch (System.ArgumentException ex)
                {
                    failure = true;
                }
                removeThese.Add(process);

                if(failure)
                    MessageBox.Show("One or more processes could not be killed. Does it still exist or do you have permission?", "Failure to kill a process!");

            }

            foreach (RunningProcess process in removeThese)
                processListBox.Items.Remove(process);


        }

        private void killAllButton_Click(object sender, EventArgs e)
        {
            bool failure = false;
            foreach (RunningProcess process in processListBox.Items)
            {
                try
                {
                    ProcessKiller.KillProcess(process);
                }
                catch (System.ArgumentException ex)
                {
                    failure = true;
                }
            }

            processListBox.Items.Clear();
            killButton.Enabled = false;

            if (failure)
                MessageBox.Show("One or more processes could not be killed. Does it still exist or do you have permission?", "Failure to kill a process!");
        }

        private void processSearchComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            Search();
        }

        private void addToTrackButton_Click(object sender, EventArgs e)
        {
            String procName = (String)processSearchComboBox.Text;
            if(procName.Length == 0)
            {
                return;
            }

            foreach(RunningProcess item in processListBox.Items) {
                stayDeadListBox.Items.Add(item.Name);
            }

            processListBox.Items.Clear();
        }

        public void MontiorDeadList()
        {
            for (;;)
            {
                foreach(String item in stayDeadListBox.Items)
                {
                    ProcessSearch search = new ProcessSearch(item);
                    List<RunningProcess> processes = search.searchProcesses();
                    if (processes.Count > 0)
                    {
                        RunningProcess process = processes.ElementAt(0);
                        ProcessKiller.KillProcess(process);
                    }
                }
                Thread.Sleep(1000);
                if(this.IsDisposed)
                {
                    break;
                }
            }
        }
    }
}
