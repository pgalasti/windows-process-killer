﻿using KillThis.Struture;
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KillThis.Logic
{
    class ProcessKiller
    {
        public static void KillProcess(RunningProcess process)
        {
            try
            {
                Process processToDie = Process.GetProcessById(process.ID);
                processToDie.Kill();
            } catch (Exception e)
            {
                String message = e.Message;
            }
        }
    }
}
