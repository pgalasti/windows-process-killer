﻿using KillThis.Struture;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace KillThis.Logic
{
    class ProcessSearch
    {
        private String searchString = String.Empty;

        public ProcessSearch(String searchString = "")
        {
            this.searchString = searchString;
        }

        public List<RunningProcess> searchProcesses()
        {
            List<RunningProcess> processes = new List<RunningProcess>();

            Process[] processList = Process.GetProcesses();
            foreach(Process process in processList)
            {
                int id = process.Id;
                String name = process.ProcessName;
                String upperName = name.ToUpper();

                if(this.searchString == String.Empty)
                {
                    processes.Add(new RunningProcess(id, name));
                }
                else if(upperName.Contains(this.searchString.ToUpper()))
                {
                    processes.Add(new RunningProcess(id, name));
                }
                
            }

            return processes;
        }

    }
}
